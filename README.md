# Simple MVC framework 
This is a simple MVC framework for building applications
in PHP. It's free and [open-source](LICENSE)

This framework was created for deep learning of the PHP: routing,
patterns, test, MVC deep understanding and other

### This framework is using:
 - Model View Controller pattern
 - ORM (Active Records)
 - Helpers
 - Laravel "Blade" as template engine
 - Dependency Injection Container
 - Optimized for PSR-7 Request and Response objects
 - Advanced routers
 - Middlewares

## Routing
The [Router](App\Core\Route.php) is translating URL into Controllers and actions.
To configure route, open [routers](App\routes.php) file and add routers. For example, to do some action by opening ``` {domain}/rss ``` URL, add next route
```php
 $router->get('rss', 'RssController@index');
```
You can also use "Advanced routing"

```php
 $router->get('post/{caregory}/{id}', 'PostController@get');
```
The URL "post/cars/1" will call method "get" of the PostController and pass "cars" and "1" as arguments of the 
method

PostController will look like this:
```php
 namespace App\Controllers;

 class Posts extends Controller
 {
    public function get($category, $id) 
    {
      // routing fill automatically pass found parameters as arguments to the method
    }
 }
```

## Middlewares

Simple MVC framework is using Middleware conception. It mean that developer can run actions on 
a request and after executed.

MVC is register a POST SanitizingMiddleware. Developer can sanitize all POST requests using this middleware:

```php
if ($request->getMethod() == 'POST') {
    //TODO: Sanitize all POST requests
}
```