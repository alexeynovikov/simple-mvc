<?php
use App\Core\Configs;
use App\View;
use Philo\Blade\Blade;

require_once __DIR__ . '/vendor/autoload.php';
$_GET['one'] = 'two';
$views = __DIR__ . '/App/templates';
$cache = __DIR__ . '/App/cache';
$blade = new Blade($views, $cache);
$app = \App\Core\Application::gi();

$config = [
    'ROOT' => __DIR__ . '/App'
];
Configs::set($config);
