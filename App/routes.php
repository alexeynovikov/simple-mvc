<?php
/* index */
$router->get('/', 'Posts@index');

/* posts */
$router->get('posts', 'Posts@index');
$router->get('post/new', 'Posts@new');
$router->post('post/new', 'Posts@store');
$router->get('post/{id}', 'Posts@show');
