<?php

namespace App;

class View
{
    protected $data = [];
    
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }
    
    public function __get($name)
    {
        return $this->data[$name];
    }
    
    public function display($template)
    {
        foreach ($this->data as $parameter => $value) {
            $$parameter = $value;
        }
        
        include $template;
    }
    
    public function render($template)
    {
        ob_start();
        
        foreach ($this->data as $parameter => $value) {
            $$parameter = $value;
        }
        
        include $template;
        $content = ob_get_contents();
        ob_end_clean();
        
        return $content;
    }
}