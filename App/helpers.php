<?php

use App\Core\Path\PathFinder;
use Philo\Blade\Blade;

if (!function_exists('view')) {
    function view($path, $data)
    {
        $views = __DIR__ . '/templates';
        $cache = __DIR__ . '/cache';
        $blade = new Blade($views, $cache);
        
        echo $blade->view()->make($path, $data)->render();
    }
}

if (!function_exists('path')) {
    /**
     * @return PathFinder
     */
    function path()
    {
        return new PathFinder();
    }
}

if (!function_exists('logger')) {
    function logger()
    {
        return new App\Core\Logger\Logger();
    }
}

if (!function_exists('redirect')) {
    function redirect($url)
    {
        $redirectObject = new \App\Core\Redirect();
        $redirectObject->redirect($url);
    }
}

if (!function_exists('app')) {
    function app()
    {
        return \App\Core\Application::gi();
    }
}

if (!function_exists('request')) {
    /**
     * @param string $param
     * @return \App\Core\Http\Request
     */
    function request($param = '')
    {
        if ($param) {
            
        }
        
        return app()->build(\App\Core\Http\Request::class);
    }
}