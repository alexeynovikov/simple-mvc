<?php
namespace App\Models;

use App\Model;

class Post extends Model
{
    const TABLE = 'Posts';
    public $Title;
    public $Body;
}
