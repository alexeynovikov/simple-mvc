<?php
namespace App\Core;

class AppExceptions
{
    public static function pageNotFound()
    {
        return view('404', []);
    }
}
