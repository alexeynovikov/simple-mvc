<?php
namespace App\Core\Http;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class Response
{
    private $psrResponse;
    
    /**
     * Response constructor.
     */
    public function __construct()
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyResponse = new SymfonyResponse('Content');
        $this->psrResponse = $psr7Factory->createResponse($symfonyResponse);
    }
    
    public function getResponse()
    {
        return $this->psrResponse;
    }
}