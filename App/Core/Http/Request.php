<?php
namespace App\Core\Http;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class Request
{
    /**
     * @var \Zend\Diactoros\ServerRequest
     */
    private $psrRequest;
    
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;
    
    /**
     * Request constructor.
     */
    public function __construct()
    {
            /* convert symfony request component to the PSR-7 */
            $psr7Factory = new DiactorosFactory();
        
            /* creating symfony request component */
            $symfonyRequest = SymfonyRequest::createFromGlobals();
            $this->request = $symfonyRequest;
        
            $psrRequest = $psr7Factory->createRequest($symfonyRequest);
            $this->psrRequest = $psrRequest;
    }
    
    /**
     * Return Symfony HTTP/Request object
     *
     * @return SymfonyRequest
     */
    public function getObj()
    {
        return $this->request;
    }
    
    /**
     * Return PSR-7 request object
     *
     * @return \Zend\Diactoros\ServerRequest
     */
    public function psr7()
    {
        return $this->psrRequest;
    }
    
    /**
     * Return prepared URI
     *
     * @return mixed|string
     */
    public function uri()
    {
        
        $uri = $this->psrRequest->getUri()->getPath();
        $url = parse_url($uri, PHP_URL_PATH);
        $uri = str_replace('index.php', '', $url);
        $uri = trim($uri, '/');
        
        return $uri;
    }
    
    /**
     * Return request method
     *
     * @return string - Method type: {POST, GET, PUT, PATCH}
     */
    public function method()
    {
        return $this->psrRequest->getServerParams()['REQUEST_METHOD'];
    }
}