<?php
namespace App\Core;

/**copyright**/
class Request
{
    
    public static function uri()
    {
        $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = str_replace('index.php', '', $url);
        $uri = trim($uri, '/');
        
        return $uri;
    }
    
    public function get($name = '')
    {
       return $this->returnRequest($_GET, $name);
    }
    
    public function post($name = '')
    {
        return $this->returnRequest($_POST, $name);
    }
    
    public function all()
    {
        return $_REQUEST;
    }
    
    public function request($name = '')
    {
        return $this->returnRequest($_REQUEST, $name);
    }
    
    public function returnRequest($type, $name)
    {
        if (!$name) {
            return $this;
        }
        
        if (isset($type[$name])) {
            return $type[$name];
        }
        
        return false;
    }
    
    public function sanitize($unsinitized)
    {
        return $unsinitized;
        //TODO: Подумать по поводу очистки запросов от атак
    }
    
    /**
     * Return request type
     */
    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}