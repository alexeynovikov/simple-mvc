<?php
namespace App\Core;

use App\Singleton;

class Configs
{
    use Singleton;
    public static $configs = [];
    
    public static function set($key, $value = '')
    {
        if (is_array($key)) {
            foreach ($key as $config => $value) {
                self::$configs[$config] = $value;
            }
            return true;
        }
        
        self::$configs[$key] = $value;
        return true;
    }
    
    public static function get($config)
    {
        return self::$configs[$config];
    }
}
