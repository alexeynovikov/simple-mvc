<?php
namespace App\Core;

class Redirect
{
    public function redirect($url)
    {
        header("Location: /index.php/{$url}");
    }
}