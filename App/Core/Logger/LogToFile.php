<?php
namespace App\Core\Logger;
use Psr\Log\LoggerInterface;

class LogToFile implements
    LoggerInterface
{
    private $file;
    private $message_pattern;
    
    const NOTICE = 'Notice';
    const ERROR  = '!ERROR';
    
    public function __construct()
    {
        $this->message_pattern = '{type} [{date}] >> {message}';
        $this->file = path()->temp('log');
    }
    
    /**
     * System is unusable.
     *
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function emergency($message, array $context = array())
    {
        // TODO: Implement emergency() method.
    }
    
    /**
     * Action must be taken immediately.
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function alert($message, array $context = array())
    {
        // TODO: Implement alert() method.
    }
    
    /**
     * Critical conditions.
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function critical($message, array $context = array())
    {
        // TODO: Implement critical() method.
    }
    
    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function error($message, array $context = array())
    {
        // TODO: Implement error() method.
    }
    
    /**
     * Exceptional occurrences that are not errors.
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function warning($message, array $context = array())
    {
        // TODO: Implement warning() method.
    }
    
    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function notice($message, array $context = array())
    {
        $str = $this->replace(self::NOTICE, date('Y-m-d H:i:s'), $message);
        $this->write($str);
    }
    
    /**
     * Interesting events.
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function info($message, array $context = array())
    {
        // TODO: Implement info() method.
    }
    
    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function debug($message, array $context = array())
    {
        // TODO: Implement debug() method.
    }
    
    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function log($level, $message, array $context = array())
    {
        // TODO: Implement log() method.
    }
    
    public function replace($type = '', $date = '', $message = '')
    {
        $find = ['{type}', '{date}', '{message}'];
        $replace = [$type, $date, $message];
        
        return str_replace($find, $replace, $this->message_pattern);
    }
    
    private function write($str)
    {
        if (!$str) {
            return false;
        }
        
        $log = fopen($this->file, 'a');
        fwrite($log, $str . PHP_EOL);
        fclose($log);
        
        return true;
    }
}
