<?php
namespace App\Core\Logger;

class Logger
{
    /**
     * @var LoggerInterface;
     */
    private $logger;
    
    /**
     * Switch logging process to the file
     *
     * @return LogToFile
     */
    public function toFile()
    {
        return $this->logger = new LogToFile();
    }
}