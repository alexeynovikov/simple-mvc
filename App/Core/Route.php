<?php
namespace App\Core;

use App\Core\Http\Request;
use Exception;

class Route
{
    /**
     * @var array - registered route
     */
    protected $routes
        = [
            'GET' => [],
            'POST' => [],
        ];
    
    /**
     * @var array - Route params
     */
    protected $params = [];
    
    /**
     * Catch all get requests.
     *
     * @param $uri - URI of the request
     * @param $controller - Assign controller to this URI
     */
    public function get($uri, $controller)
    {
        $route_info = $this->analyzeRoute($uri);
        $this->routes['GET'][$route_info['regex']] = $controller;
        $this->params['GET'][$route_info['regex']] = $route_info['route_arguments'];
    }
    
    /**
     * Catch all post requests.
     *
     * @param $uri - URI of the request
     * @param $controller - Assign controller to this URI
     */
    public function post($uri, $controller)
    {
        $route_info = $this->analyzeRoute($uri);
        $this->routes['POST'][$route_info['regex']] = $controller;
        $this->params['POST'][$route_info['regex']] = $route_info['route_arguments'];
    }
    
    /**
     * @return array of the routes
     */
    public function getRoutes()
    {
        return $this->routes;
    }
    
    /**
     * Detect routes.
     *
     * @param $url - URL of the request
     * @return mixed - route information
     * @throws Exception - an extension if route info doesn't found
     */
    public function direct($url, $requestType)
    {
        foreach ($this->routes[$requestType] as $key => $actionMask) {
            $action_array = explode('@', $actionMask);
           
            /* index route handling */
            if($key == '/') {
                if ($url == '') {
                    return $this->callAction($action_array[0], $action_array[1]);
                }
                continue;
            }
            
            if (preg_match("~{$key}~", $url, $arguments)) {
                array_shift($arguments);
                
                /* fill request with arguments */
                $this->buildArgs($this->params[$requestType][$key], $arguments, $requestType);
                return $this->callAction($action_array[0], $action_array[1], $arguments);
            }
        }
        
        return AppExceptions::pageNotFound();
    }
    
    public function buildArgs($route_args, $uri_args, $request_type)
    {
        foreach ($route_args as $key => $arg) {
            $value = $uri_args[$key];
            
            switch ($request_type) {
                case 'GET':
//                    $request = request()->psr7()->withQueryParams([$arg => $value]);
                    break;
                case 'POST':
                    $GLOBALS['_POST'][$arg] = $value;
                    break;
            }
        }
        
    }
    
    private function analyzeRoute($framework_pattern)
    {
        $re = '({[a-z]+})';
        $any = "([a-zA-Z0-9_.-]+)";
        
        
        /* find arguments */
        preg_match_all("~{$re}~", $framework_pattern, $route_arguments);
        array_shift($route_arguments);
        foreach ($route_arguments[0] as $key => $route) {
            $sanitized_arg = str_replace(['{', '}'], '', $route);
            $route_arguments[0][$key] = $sanitized_arg;
        }
        
        /* build route matching regex */
        $regex = preg_replace("~{$re}~", $any, $framework_pattern);
        
        return [
            'route_arguments' => $route_arguments[0],
            'regex' => $regex
        ];
    }
    
    public function dispatchRoute($url, $requestType)
    {
//        $url
    }
    
    /**
     * Load file.
     *
     * @param $file - Path of the file which you wan't to include
     * @return static
     */
    public static function load($file)
    {
        $router = new static();
        require $file;
        
        return $router;
    }
    
    
    
    public function callAction($contoller, $action, $arguments = [])
    {
        $contoller = "App\\Controllers\\{$contoller}";
        $contoller = new $contoller;
        
        if (method_exists($contoller, $action)) {
            return (new $contoller)->$action(...$arguments);
        } else {
            throw new Exception("Method {$action} doesn't found");
        }
    }
}