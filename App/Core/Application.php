<?php
namespace App\Core;

use App\Core\Http\Request;
use App\Core\Http\Response;
use Idealo\Middleware\Stack;
use App\Core\Containers\Container;
use App\Core\Middlewares\SanitizingMiddleware;

class Application
{
    protected $_classes;
    protected $_components;
    protected $_middlewares;
    
    /**
     * @var string - MVC Framework name {not final - need to think}
     */
    public $name = 'tiny';
    
    /**
     * @var string - Framework version
     */
    public $version = '0.0.2';
    
    public static $instance;
    private $middlewareIntance;
    
    /**
     * @var array|\stdClass - Additional components of the application
     */
//    private $_components = [];
    
    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->_components = new \stdClass();
        $this->_classes = new \stdClass();
        $this->singletones = new \stdClass();
        
        $this->setCoreClasses();
        $this->getCoreMiddlewares();
        $this->listenMiddlewares();
    }
    
    public function getCoreMiddlewares()
    {
        $this->_middlewares = [
          SanitizingMiddleware::class
        ];
    }
    
    public function listenMiddlewares()
    {
        $request = $this->build(Request::class);

        $defaultResponse = $this->build(Response::class);
        $this->middlewareIntance = new Stack(
            $defaultResponse->getResponse(),
            new SanitizingMiddleware()
        );

        $this->middlewareIntance->process($request->psr7());
    }
    
    public static function gi()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
    
        return self::$instance;
    }
    
    public function getLoadedComponents()
    {
        return $this->_components;
    }
    
    public function getLoadedClasses()
    {
        return $this->_classes;
    }
    
    /**
     * @param  string $className - Class name with
     * @return object            - Asked class instance
     */
    public function __get($className)
    {
        return $this->resolve($className);
    }
    
    /**
     * Get basic core classes
     * @return array - List of the main classes
     */
    public function getCoreClasses()
    {
        return [
            \App\Core\Http\Request::class,
            \App\Core\Http\Response::class
        ];
    }
    
    /**
     * Build asked class, if it exist in the list of the loaded classes
     *
     * @param  string $className - Full class name including namespaces
     * @return object            - Instance of this class
     */
    public function build($className)
    {
        return $this->resolve($className);
    }
    
    /**
     * Resolve and return asked instance of the class
     *
     * @param  string $className  - Class name
     * @return object             - Instance of the asked class
     * @throws \Exception         - Class not found Exception
     */
    public function resolve($className)
    {
        if (isset($this->_components->{$className})) {
            return $this->_components->{$className};
        }
    
        if (!class_exists($className)) {
            throw new \Exception("Component {$className} not found!");
            //TODO: Throw own exception
        }
    
        if (method_exists($className, '__constructor') !== false) {
            $refMethod = new \ReflectionMethod($className, '__constructor');
            $params = $refMethod->getParameters();
        
            $re_args = [];
        
            foreach ($params as $key => $param) {
                if ($param->isDefaultValueAvailable()) {
                    $re_args[$param->name] = $param->getDefaultValue();
                } else {
                    $class = $param->getClass();
                    if (null != $class) {
                        $re_args[$param->name] = $this->{$class->name};
                    } else {
                        throw new \Exception("Not found {$class->name} in container");
                        //TODO: Throw own exception
                    }
                }
            }
        
            $refClass = new \ReflectionClass($className);
            $class_instance = $refClass->newInstanceArgs((array)$re_args);
        } else {
            $class_instance = new $className();
        }
    
        return $this->_components->{$className} = $class_instance;
    }
    
    /**
     * Prepare core classes to be build
     *
     * @param  array   $components - Application additional components
     * @return object              - List of the classes that can be Injected
     */
    public function setCoreClasses($components = [])
    {
        foreach ($this->getCoreClasses() as $class) {
            $this->resolve($class);
        }
//        $this->_components = (object)array_merge($this->getCoreClasses(), $components);
    }
}