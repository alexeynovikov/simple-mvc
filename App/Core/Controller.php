<?php
namespace  App\Core;

class Controller
{
    
    public function validate($data)
    {
        $has_error = false;
        foreach ($data as $key => $value) {
            if (empty($value)) {
                $has_error = true;
                break;
            }
        }
    
        if ($has_error) {
            redirect('/');
        }
        //TODO: Переписать валидацию. Текущий вид валидации - временный. Нужно написать более гибкую валидацию
    }
}