<?php
namespace App\Core\Path;

use App\Core\Configs;

class PathFinder
{
    
    private $path;
    
    public function __construct()
    {
        $this->path = Configs::get('ROOT');
    }
    
    public function temp($file)
    {
        switch ($file) {
            case 'log':
                $file = 'log.log';
                $log_file = $this->path . '/tmp/' . $file;
                
                if (!file_exists($log_file)) {
                    touch($log_file);
                }
                break;
        }
        $this->path .= '/tmp/' . $file;
        
        return $this->give();
    }
    
    public function view($file)
    {
        $this->path .= '/templates/' . $file;
        return $this->give();
    }
    
    public function give()
    {
        if (file_exists($this->path)) {
            return $this->path;
        }
        
        return false;
    }
}