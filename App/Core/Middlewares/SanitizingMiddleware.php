<?php
namespace App\Core\Middlewares;

use \Interop\Http\ServerMiddleware\DelegateInterface;
use \Interop\Http\ServerMiddleware\MiddlewareInterface;
use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;

class SanitizingMiddleware implements MiddlewareInterface
{
    
    /**
     * Process an incoming server request and return a response, optionally delegating
     * to the next middleware component to create the response.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface      $delegate
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate) : ResponseInterface
    {
        try {
            if ($request->getMethod() == 'POST') {
                //TODO: Sanitize all POST requests
            }
        } catch (\Exception $exception) {
            
        }

        return $delegate->process($request);
    }
}