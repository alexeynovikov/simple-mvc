<?php
namespace App\Core;

class NotFound
{
    public function index()
    {
        return view('404', []);
    }
}