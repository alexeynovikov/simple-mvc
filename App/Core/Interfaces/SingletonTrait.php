<?php
namespace App\Core\Interfaces;

trait Singleton
{
    protected static $instance;
    
    protected function __construct() {}
    
    public static function instance()
    {
        if (null === static::$instance) {
            return new static();
        }
        
        return static::instance();
    }
    
    protected function __clone() {}
}