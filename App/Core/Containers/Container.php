<?php
namespace App\Core\Containers;

class Container
{
    protected $_classes;
    protected $_components;
    protected $_middlewares;
    public static $instance;
    
    public function flushAll()
    {
        $this->_classes = new \stdClass();
        $this->_components = new \stdClass();
        $this->_middlewares = new \stdClass();
    }
    
    public static function gi()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function getLoadedClasses()
    {
       return $this->_classes;
    }
    
    public function getLoadedComponents()
    {
        return $this::gi()->_components;
    }
    
}