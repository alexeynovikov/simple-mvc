<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Http\Request;
use App\Models\Post;

class Posts extends
    Controller
{
    /**
     * Get and render all posts
     */
    public function index()
    {
        $posts = Post::findAll();
        
        return view('index', compact('posts'));
    }
    
    /**
     * Showing posts page
     */
    public function show($id)
    {
        $post = Post::findByID($id);
        
        return view('post.single', compact('post'));
    }
    
    public function edit($category, $post_id)
    {
        
    }
    
    /**
     * Showing new post form
     */
    public function new()
    {
        return view('post.new', []);
    }
    
    /**
     * Save new post object
     */
    public function store()
    {
        $data = request()->psr7()->getParsedBody();
        $title = $data['postTitle'];
        $body = $data['postBody'];
        
        $post = new Post();
        $post->Title = $title;
        $post->Body = $body;
        
        $post->save();
        
        return redirect('/');
    }
    
}