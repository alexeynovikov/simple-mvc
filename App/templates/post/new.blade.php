@extends('master')

@section('content')
    <form method="POST" action="/post/new">
        <div class="form-group">
            <label for="postTitle">Title</label>
            <input name="postTitle" type="text" class="form-control" id="postTitle" placeholder="Enter title">
        </div>

        <div class="form-group">
            <label for="postBody">Body</label>
            <textarea name="postBody" class="form-control" id="postBody" rows="3"></textarea>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
    </form>
@endsection