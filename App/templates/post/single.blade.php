@extends('master')

@section('content')
    @if($post)
        <h1>{{$post->Title}}</h1>
        <section>
            {{$post->Body}}
        </section>
        @else
        <h1>Post didn't found</h1>
    @endif

@endsection