@include('layouts.header')

@include('layouts.nav')

<div class="container">
    <div class="row">
        <div class="col-sm-8 blog-main">
            @yield('content')
        </div><!-- /.blog-main -->

        @include('layouts.sidebar') <!-- blog sidebar -->
    </div><!-- /.row -->
</div><!-- /.container -->

@include('layouts.footer')