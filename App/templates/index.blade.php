@extends('master')

@section('content')
    @if(isset($posts))
        @foreach ($posts as $post)
            <div class="blog-post">
                <h2 class="blog-post-title">{{$post->Title}}</h2>
                {{$post->Body}}
                <div style="text-align: right">
                    <a class="btn btn-outline-primary" href="/post/{{$post->id}}">Read</a>
                </div>

            </div><!-- /.blog-post -->
        @endforeach

        <nav class="blog-pagination">
            <a class="btn btn-outline-primary" href="#">Older</a>
            <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
        </nav>
    @else
        There is any posts
    @endif

@endsection