<?php

namespace App;

use App\Models\User;

class Db
{
    use Singleton;
    
    protected $dbh;
    
    protected function __construct()
    {
        $this->dbh = new \PDO('mysql:host=127.0.0.1;dbname=mvc1', 'root', 12345);
    }
    
    public function execute($sql, $params)
    {
        $sth = $this->dbh->prepare($sql);
        $res = $sth->execute($params);
        return $res;
    }
    
    public function query($sql, $class_name)
    {
        $sth = $this->dbh->prepare($sql);
        $res = $sth->execute();
    
        if (false !== $res) {
            return $sth->fetchAll(\PDO::FETCH_CLASS, $class_name);
        }
        
        return [];
    }
}