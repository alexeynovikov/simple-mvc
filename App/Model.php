<?php
namespace App;

abstract class Model
{
    const TABLE = '';
    public $id;
    
    public static function findAll()
    {
        $db = Db::instance();
        return $db->query('SELECT * FROM ' . static::TABLE, static::class);
    }
    
    public static function findByID(int $id)
    {
        $db = Db::instance();
        $res = $db->query("SELECT * FROM " . static::TABLE . " WHERE `id` = {$id}", static::class);
        
        return !empty($res) ? $res[0] : $res;
    }
    
    public static function findByEmail(string $email)
    {
        $db = Db::instance();
        return $db->query("SELECT * FROM {static::TABLE} WHERE `email` = {$email}", static::class);
    }
    
    public function isNew()
    {
        return empty($this->id);
    }
    
    public function insert()
    {
        if (!$this->isNew()) {
            return;
        }
        
        $colums = [];
        $values = [];
        foreach ($this as $key => $value) {
            if ('id' == $key) {
                continue;
            }
            
            $colums[] = $key;
            $values[':' . $key] = $value;
        }
        $colums = implode(',', $colums);
        $values_str = implode(',', array_keys($values));
        $sql = "INSERT INTO `" . static::TABLE . "` ({$colums}) VALUES ({$values_str})";
        
        $db = Db::instance();
        return $db->execute($sql, $values);
    }
    
    public function update()
    {
        
    }
    
    public function delete()
    {
        
    }
    
    public function save()
    {
        return $this->insert();
    }
}